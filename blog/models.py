from django.db import models
from django.utils import timezone
from django.shortcuts import reverse

class Category(models.Model):
    title = models.CharField('Заголовок', max_length=255 )
    slug = models.SlugField('Ссылка', max_length=255, unique=True)
    image = models.ImageField('Картинка', blank=True)
    

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug':self.slug})
        
    class Meta:
        verbose_name="Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.title





class Post(models.Model):
    title = models.CharField('Загаловок', max_length=255 )
    slug = models.SlugField('Сcылка', max_length=255, unique=True)
    summary = models.TextField('Краткое Описание')
    content = models.TextField('Описаниe')
    date = models.DateTimeField('Дата', default = timezone.now)
    image = models.ImageField('Картинка', blank=True)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    views = models.IntegerField('Просмотры', default=0)



    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug':self.slug})

    class Meta:
        verbose_name="Пост"
        verbose_name_plural="Посты"

    def __str__(self):
        return self.title

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    author_name = models.CharField("Имя Автора", max_length=255)
    text = models.TextField("Текст Коментарии")
    date = models.DateTimeField("Дата коментария", default=timezone.now)

    class Meta:
        verbose_name = "Коментарий"
        verbose_name_plural = "Коментраии"

    def __str__(self):
        return self.author_name

class FeedBack(models.Model):
    name = models.CharField("Имя", max_length=255)
    last_name = models.CharField("Фамилия", max_length=255)
    email = models.EmailField("Ваш эмайл")
    phone = models.CharField("Телефон номера", max_length=255)
    message = models.TextField("Сообщение")
    date = models.DateTimeField("Дата", default=timezone.now)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
            return self.name

        