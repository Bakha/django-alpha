from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('blog/', views.blog, name="blog"),
    path('search/', views.search_results, name="search_results"),
    path('post/<str:slug>', views.post_detail, name="post_detail_url"),
    path('category/<slug:slug>/', views.category_detail, name="category_detail_url"),
    path('categories/', views.Categories_detail, name="categories_detail_url"),
    path('register/', views.register, name="register"),
    path('post/<slug:slug>/leave-comment', views.leave_comment, name='leave_comment'),
    path('contact/', views.contact, name="contact"),
    path('populars/', views.populars, name="populars"),
]
